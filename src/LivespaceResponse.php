<?php
/**
 * Copyright (c) Livespace sp. z o.o. (https://livespace.io)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) 2017, Livespace sp. z o.o. (https://livespace.io)
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

namespace Livespace;

class LivespaceResponse
{
    const DATA_INDEX = 'data';
    const RESULT_INDEX = 'result';
    const STATUS_INDEX = 'status';
    const ERROR_INDEX = 'error';


    /**
     * @var array
     */
    protected $_responseData = array();


    /**
     * @param array $responseData
     */
    public function __construct(array $responseData = null)
    {
        if ($responseData) {
            $this->setResponseData($responseData);
        }
    }

    /**
     * @param $responseData
     * @return LivespaceResponse
     */
    public function setResponseData($responseData)
    {
        $this->_responseData = $responseData;
        return $this;
    }

    /**
     * @return array
     */
    public function getResponseData()
    {
        return $this->_responseData;
    }

    /**
     * @return null|array
     */
    public function getData()
    {
        return $this->_getData(static::DATA_INDEX);
    }

    /**
     * @return null|int
     */
    public function getResult()
    {
        return $this->_getData(static::RESULT_INDEX);
    }

    /**
     * @return null|bool
     */
    public function getStatus()
    {
        return $this->_getData(static::STATUS_INDEX);
    }

    /**
     * @return null|array
     */
    public function getError()
    {
        return $this->_getData(static::ERROR_INDEX);
    }

    /**
     * @param $key
     * @return mixed
     */
    protected function _getData($key)
    {
        return array_key_exists($key, $this->_responseData)
            ? $this->_responseData[$key]
            : null;
    }
}