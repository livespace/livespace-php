<?php
/**
 * Copyright (c) Livespace sp. z o.o. (https://livespace.io)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) 2017, Livespace sp. z o.o. (https://livespace.io)
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

namespace Livespace;

class LivespaceSerializeException extends LivespaceException
{
}

interface LivespaceSerializeInterface
{
    /**
     * @param $data
     * @return mixed
     */
    public function serialize($data);

    /**
     * @param $data
     * @return mixed
     */
    public function unserialize($data);
}

class LivespaceSerializePhp implements LivespaceSerializeInterface
{
    /**
     * @param $data
     * @return string
     */
    public function serialize($data)
    {
        return serialize($data);
    }

    /**
     * @param $data
     * @return mixed
     * @throws LivespaceSerializeException
     */
    public function unserialize($data)
    {
        $result = unserialize($data);
        if (false === $result) {
            throw new LivespaceSerializeException('Cannot unserialize data.');
        }
        return $result;
    }
}

class LivespaceSerializeJson implements LivespaceSerializeInterface
{
    /**
     * @param $data
     * @return string
     */
    public function serialize($data)
    {
        return json_encode($data);
    }

    /**
     * @param $data
     * @return mixed
     * @throws LivespaceSerializeException
     */
    public function unserialize($data)
    {
        $result = json_decode($data, true);
        if (null === $result) {
            throw new LivespaceSerializeException('Cannot unserialize data.');
        }
        return $result;
    }
}

class LivespaceSerialize
{
    /**
     * @param $format
     * @return mixed
     * @throws LivespaceSerializeException
     */
    public static function factory($format)
    {
        $className = '\Livespace\LivespaceSerialize' . ucfirst(strtolower($format));
        if (!class_exists($className)) {
            throw new LivespaceSerializeException('Serialize class for format \'' . $format . '\' does not exists.');
        }
        return new $className();
    }

    /**
     * @param $data
     * @param $format
     * @return mixed
     * @throws LivespaceSerializeException
     */
    public static function serialize($data, $format)
    {
        return static::factory($format)->serialize($data);
    }

    /**
     * @param $data
     * @param $format
     * @return mixed
     * @throws LivespaceSerializeException
     */
    public static function unserialize($data, $format)
    {
        return static::factory($format)->unserialize($data);
    }
}