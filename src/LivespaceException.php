<?php
/**
 * Copyright (c) Livespace sp. z o.o. (https://livespace.io)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) 2017, Livespace sp. z o.o. (https://livespace.io)
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

namespace Livespace;

use Exception;

class LivespaceException extends Exception
{
}