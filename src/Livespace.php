<?php
/**
 * Copyright (c) Livespace sp. z o.o. (https://livespace.io)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) 2017, Livespace sp. z o.o. (https://livespace.io)
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

namespace Livespace;

/**
 * Livespace PHP SDK
 */
class Livespace
{
    /**
     * SDK Version
     */
    const VERSION = '1.0';

    /**
     * URL pattern
     */
    const API_URL_PATTERN = '%s/api/public/%s/%s';

    /**
     * Error msg pattern
     */
    const CURL_ERROR_PATTERN = 'Curl error #%d msg: %s';


    /**
     * Default options for curl.
     */
    protected static $_CURL_OPTS = array(
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_USERAGENT => 'livespace-sdk-php-' . self::VERSION,
        CURLOPT_POST => true,
        CURLOPT_FAILONERROR => true,
        CURLOPT_HEADER => false
    );

    /**
     * @var array
     */
    protected static $_OPTIONS_DEFAULT = array(
        'format' => 'json',
        'auth_method' => 'key',
        'return_raw' => false
    );

    /**
     * @var array
     */
    protected $_options = array();


    /**
     * @param $options
     */
    public function __construct($options)
    {
        $this->init($options);
    }

    /**
     * @param $options
     * @throws LivespaceException
     */
    public function init($options)
    {
        $this->setOptions($options);
    }

    /**
     * @param $options
     * @return Livespace
     * @throws LivespaceException
     */
    public function setOptions($options)
    {
        if (!is_array($options)) {
            throw new LivespaceException('Options should be an array.');
        }
        $this->_options = $options;
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return Livespace
     */
    public function setOption($key, $value)
    {
        $this->_options[$key] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * @param $key
     * @param null $defaultValue
     * @return null
     */
    public function getOption($key, $defaultValue = null)
    {
        return array_key_exists($key, $this->_options)
            ? $this->_options[$key]
            : $defaultValue;
    }

    /**
     * @param $action
     * @param array $params
     * @param array $additionalOptions
     * @return LivespaceResponse|mixed
     * @throws LivespaceSerializeException|LivespaceException
     */
    public function call($action, $params = array(), $additionalOptions = array())
    {
        if (!is_array($params)) {
            $params = array();
        }

        $oldOptions = $this->getOptions();
        if (!is_array($additionalOptions)) {
            $additionalOptions = array();
        }
        $this->setOptions(array_merge($oldOptions, $additionalOptions));

        $format = $this->getOption('format', static::$_OPTIONS_DEFAULT['format']);

        if ('none' != $this->getOption('auth_method', static::$_OPTIONS_DEFAULT['auth_method'])) {
            $className = '\Livespace\LivespaceAuthorize' . ucfirst(
                    strtolower($this->getOption('auth_method', static::$_OPTIONS_DEFAULT['auth_method']))
                );
            if (!class_exists($className)) {
                throw new LivespaceSerializeException('Authorize class for method \'' . $this->getOption('auth_method', 'none') . '\' does not exists.');
            }

            /**
             * @var LivespaceAuthorize
             */
            $auth = new $className($this);
            $result = $auth->call($action, $format, $params);
        } else {
            $result = $this->_call($action, $format, $params);
        }

        if (false === $this->getOption('return_raw', static::$_OPTIONS_DEFAULT['return_raw'])) {
            $result = new LivespaceResponse(LivespaceSerialize::unserialize($result, $format));
        }

        $this->setOptions($oldOptions);

        return $result;
    }

    /**
     * @param $action
     * @param $format
     * @param array $params
     * @return bool|string
     * @throws LivespaceException
     */
    public function _call($action, $format, $params = array())
    {
        if (!$this->getOption('api_url') || !$this->getOption('api_url')) {
            throw new LivespaceException('Options \'api_url\' is required.');
        }

        $opts = static::$_CURL_OPTS;
        $opts[CURLOPT_URL] = sprintf(static::API_URL_PATTERN, $this->getOption('api_url'), $format, $action);
        $opts[CURLOPT_POSTFIELDS] = http_build_query($params);

        if (isset($opts[CURLOPT_HTTPHEADER])) {
            $currentHttpHeaders = $opts[CURLOPT_HTTPHEADER];
            $currentHttpHeaders[] = 'Expect:';
            $opts[CURLOPT_HTTPHEADER] = $currentHttpHeaders;
        } else {
            $opts[CURLOPT_HTTPHEADER] = array('Expect:');
        }

        if (substr($this->getOption('api_url'), 0, 5) == 'https') {
            $opts[CURLOPT_SSL_VERIFYPEER] = false;
            $opts[CURLOPT_SSL_VERIFYHOST] = 2;
        }

        $ch = curl_init();
        curl_setopt_array($ch, $opts);
        $result = curl_exec($ch);

        if (false === $result) {
            $errorMsg = sprintf(self::CURL_ERROR_PATTERN, curl_errno($ch), curl_error($ch));
            curl_close($ch);
            throw new LivespaceException($errorMsg);
        }

        curl_close($ch);

        return $result;
    }
}